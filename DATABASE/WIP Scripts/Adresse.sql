USE krautundrueben;

CREATE TABLE ADRESSE (
	ADRESSEID INTEGER NOT null,
	STRASSE VARCHAR (50),
	HAUSNR VARCHAR (6),
	PLZ VARCHAR (5),
	ORT INTEGER,
	ZUSATZ VARCHAR (100)
);

CREATE TABLE ORT (ORTID INTEGER NOT null, ORT VARCHAR (50));

/******************************************************************************/
/***                              Primary Keys                              ***/
/******************************************************************************/
ALTER TABLE
	ADRESSE
ADD
	PRIMARY KEY (ADRESSEID);

ALTER TABLE
	ORT
ADD
	PRIMARY KEY (ORTID);

/******************************************************************************/
/***                              Inhalt Ort                                ***/
/******************************************************************************/
INSERT INTO
	Ort (ORTID, ORT)
VALUES
	(0001, 'Hamburg');

INSERT INTO
	Ort (ORTID, ORT)
VALUES
	(0002, 'Barsbüttel');

INSERT INTO
	Ort (ORTID, ORT)
VALUES
	(0003, 'Weseby');

INSERT INTO
	Ort (ORTID, ORT)
VALUES
	(0004, 'Jork');

INSERT INTO
	Ort (ORTID, ORT)
VALUES
	(0005, 'Dechow');

/******************************************************************************/
/***                              Inhalt Adresse                            ***/
/******************************************************************************/
INSERT INTO
	Adresse (AdresseID, Strasse, hausnr, plz, ort)
VALUES
	(2001,'Eppendorfer Landstrasse','104','20249',0001);

INSERT INTO
	Adresse (AdresseID, Strasse, hausnr, plz, ort)
VALUES
	(2002, 'Ohmstraße', '23', '22765', 0001);

INSERT INTO
	Adresse (AdresseID, Strasse, hausnr, plz, ort)
VALUES
	(2003, 'Bilser Berg', '6', '20459', 0001);

INSERT INTO
	Adresse (AdresseID, Strasse, hausnr, plz, ort)
VALUES
	(2004, 'Alter Teichweg', '95', '22049', 0001);

INSERT INTO
	Adresse (AdresseID, Strasse, hausnr, plz, ort)
VALUES
	(2005, 'Stübels', '10', '22835', 0002);

INSERT INTO
	Adresse (AdresseID, Strasse, hausnr, plz, ort)
VALUES
	(2006, 'Grotelertwiete', '4a', '21075', 0001);

INSERT INTO
	Adresse (AdresseID, Strasse, hausnr, plz, ort)
VALUES
	(2007, 'Küstersweg', '3', '21079', 0001);

INSERT INTO
	Adresse (AdresseID, Strasse, hausnr, plz, ort)
VALUES
	(2008,'Neugrabener Bahnhofstraße', '30', '21149', 0001);

INSERT INTO
	Adresse (AdresseID, Strasse, hausnr, plz, ort)
VALUES
	(2009,'Elbchaussee', '228', '22605', 0001);

INSERT INTO
	Adresse (AdresseID, Strasse, hausnr, plz, ort)
VALUES
	(1001, 'Dorfstraße', '74', '24354', 0003);

INSERT INTO
	Adresse (AdresseID, Strasse, hausnr, plz, ort)
VALUES
	(1002, 'Westerjork', '76', '21635', 0004);

INSERT INTO
	Adresse (AdresseID, Strasse, hausnr, plz, ort)
VALUES
	(1003, 'Molkereiweg', '13', '19217', 0005);