/* Folgende "Variabeln" werden benutzt
RezeptID
KundenID
*/

use krautundrueben;

INSERT INTO 
    bestellung (KUNDENNR,BESTELLDATUM)
    VALUES
    (KundenID,current_timestamp);

UPDATE bestellung
    SET ADRESSEID =
        (SELECT ADRESSEID FROM kunde 
            WHERE KUNDENNR = KUNDENID)
        WHERE BESTELLNR = LAST_INSERT_ID();

UPDATE bestellung
    SET RECHNUNGSBETRAG =
        (
    SELECT SUM(NETTOPREIS * MENGE) FROM REZEPTZUTAT 
    LEFT JOIN Zutat 
    ON REZEPTZUTAT.ZUTATENNR=ZUTAT.ZUTATENNR 
    WHERE REZEPTNR=ID)
    WHERE BESTELLNR = LAST_INSERT_ID();

INSERT INTO bestellungzutat (BESTELLNR, ZUTATENNR, MENGE)
    SELECT LAST_INSERT_ID(),rzt.ZUTATENNR,rzt.MENGE
    FROM rezeptzutat rzt 
    WHERE REZEPTNR = RezeptID;
