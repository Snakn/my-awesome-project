-- Auswahl Rezept nach Name

SELECT * FROM rezept WHERE rezept.rezeptname = 'Hasenfutter';

-- Auswahl Rezept nach beinhalteter Zutat

SELECT * FROM rezept
	INNER JOIN rezeptzutat ON rezept.REZEPTNR = rezeptzutat.REZEPTNR
	INNER JOIN zutat ON zutat.ZUTATENNR = rezeptzutat.ZUTATENNR
	WHERE zutat.BEZEICHNUNG = 'Ei';

-- Durchschnitt der Nährwerte pro Bestellung	

SELECT SUM(zutat.KALORIEN*bestellungzutat.menge)/COUNT(DISTINCT bestellung.BESTELLNR) AS 'Durchschnitt Kalorien',
	SUM(zutat.KOHLENHYDRATE*bestellungzutat.menge)/COUNT(DISTINCT bestellung.BESTELLNR) AS 'Durchschnitt Kohlenhydrate',
	SUM(zutat.PROTEIN*bestellungzutat.menge)/COUNT(DISTINCT bestellung.BESTELLNR) AS 'Durchschnitt Protein'
	FROM zutat
	LEFT JOIN bestellungzutat ON zutat.ZUTATENNR = bestellungzutat.ZUTATENNR
	INNER JOIN bestellung ON bestellung.BESTELLNR = bestellungzutat.BESTELLNR
	WHERE bestellung.KUNDENNR = '2004';

-- Auswahl aller Rezepte mit weniger als 5 Zutaten.

Select * From Rezept as ori
WHERE 
(SELECT COUNT(rezept.REZEPTNR) FROM rezept
LEFT JOIN rezeptzutat
ON rezeptzutat.REZEPTNR = rezept.REZEPTNR
WHERE rezept.REZEPTNR = ori.REZEPTNR) < 5;

--Auswahl aller Rezepte mit weniger als 5 Zutaten und dem Ernaerungstrend = 2

SELECT * FROM rezept AS ori
WHERE 
((SELECT COUNT(rezept.REZEPTNR) FROM rezept
LEFT JOIN rezeptzutat
ON rezeptzutat.REZEPTNR = rezept.REZEPTNR
WHERE rezept.REZEPTNR = ori.REZEPTNR)
=
(SELECT COUNT(REZEPTZUTAT.Zutatennr)
FROM TRENDZUORDNUNG
right JOIN rezeptzutat
ON trendzuordnung.ZUTATENNR = REZEPTZUTAT.ZUTATENNR
INNER JOIN rezept 
ON REZEPTZUTAT.REZEPTNR = rezept.REZEPTNR WHERE trendzuordnung.trendnr = 2 AND rezept.REZEPTNR = ori.REZEPTNR)) AND
(SELECT COUNT(rezept.REZEPTNR) FROM rezept
LEFT JOIN rezeptzutat
ON rezeptzutat.REZEPTNR = rezept.REZEPTNR
WHERE rezept.REZEPTNR = ori.REZEPTNR) < 5;

-- Auswahl aller Rezepte mit weniger Kalorien als X = 2000

Select * From Rezept
Where Kalorien < 2000;

-- Auswahl aller Zutaten die keinem Rezept zugeordnet sind :

Select * From Zutat
Where 
(Select Count(rezeptzutat.zutatennr) From rezeptzutat
where rezeptzutat.zutatennr = Zutat.zutatennr) = 0;
