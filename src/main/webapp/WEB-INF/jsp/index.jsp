<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 
<html>
<head>
</head>


	<body>
		<h1>Kraut und R�ben</h1>
		
		<div class="Kundennr-input">
			<form method = "GET">
				
				<select name="trend">
					<c:forEach items="${trends}" var="etrend">
						<option id="${etrend.trendID}" value="${etrend.trendID}">${etrend.trendName}</option>
					</c:forEach>
				</select>
				<input type="submit" value="Absenden"></input>
				
				<div>
					<table border="1">
						<tr>
							<th>Rezeptnr:</th>
							<th>Rezeptname:</th>							
						</tr>
						 
						 <c:forEach items="${rezepte}" var="rezept">
							<tr>
								<td>${rezept.rezeptNummer}</td>
								<td>${rezept.rezeptName}</td>
							</tr>
						</c:forEach>
					</table>
				</div>
				
			</form>
		</div>
	</body>

</html>