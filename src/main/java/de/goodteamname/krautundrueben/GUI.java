package de.goodteamname.krautundrueben;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

public class GUI extends Frame {
	  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// Anfang Attribute
	  private Choice choice1 = new Choice();
	  private Choice recipeOrIngredient = new Choice();
	  private Button button1 = new Button("Bestellen");
	  private Button button2 = new Button();
	  private Button button3 = new Button();
	  private Button button4 = new Button();
	  private Button intoShoppingCart = new Button();
	  //private Canvas canvas1 = new Canvas();
	  //private Canvas canvas2 = new Canvas();
	  private TextField textField1 = new TextField("2001");
	  private TextField textField2 = new TextField("2002");
	  private TextField shoppingCartID = new TextField("0");
	  private TextField amount = new TextField("1");
	  private Label label1 = new Label();
	  private Label label2 = new Label();
	  private Label label3 = new Label();
	  private Label shoppingCart = new Label();
	  private Label kundBestLabel = new Label();
	  private Label bestellen = new Label();
	  private Canvas canvas3 = new Canvas();
	  private List rezeptList = new List();
	  private List zutatenList = new List();
	  private List kundBestList = new List();
	  private List shoppingCartList = new List();
	  Postman Postman = new Postman("localhost", 8080);
	  // Ende Attribute
	  
	  public GUI() { 
	    // Frame-Initialisierung
	    super();
	    addWindowListener(new WindowAdapter() {
	      public void windowClosing(WindowEvent evt) { dispose(); }
	    });
	    int frameWidth = 1113; 
	    int frameHeight = 680;
	    setSize(frameWidth, frameHeight);
	    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
	    int x = (d.width - getSize().width) / 2;
	    int y = (d.height - getSize().height) / 2;
	    setLocation(x, y);
	    setTitle("KrautControll & Rübinator");
	    setResizable(false);
	    Panel cp = new Panel(null);
	    add(cp);
	    // Anfang Komponenten
	    choice1.setBounds(200, 60, 150, 20);
	    cp.add(choice1);
	    for (int i = 0; i < Postman.trends().length; i++) {
	    	choice1.add(Postman.trends()[i]);
	    }
	    recipeOrIngredient.setBounds(750, 60, 150, 20);
	    cp.add(recipeOrIngredient);
	    recipeOrIngredient.add("Rezept");
	    recipeOrIngredient.add("Zutat");

	    
	    button1.setBounds(104, 576, 125, 25);
	    button1.setLabel("Kunden Bestellungen");
	    button1.addActionListener(new ActionListener() { 
	      public void actionPerformed(ActionEvent evt) { 
	        button1_ActionPerformed(evt);
	      }
	    });
	    cp.add(button4);
	    button4.setBounds(104, 576, 125, 25);
	    button4.setLabel("Rezept Übersicht");
	    button4.addActionListener(new ActionListener() { 
	      public void actionPerformed(ActionEvent evt) { 
	        button4_ActionPerformed(evt);
	      }
	    });
	    button4.setVisible(false);
	    cp.add(button1);
	    button2.setBounds(380, 90, 75, 25);
	    button2.setLabel("auswählen");
	    button2.addActionListener(new ActionListener() { 
	      public void actionPerformed(ActionEvent evt) { 
	        button2_ActionPerformed(evt);
	      }
	    });
	    cp.add(button2);
	    button3.setBounds(960, 576, 75, 25);
	    button3.setLabel("Bestellen");
	    button3.addActionListener(new ActionListener() { 
	      public void actionPerformed(ActionEvent evt) { 
	        button3_ActionPerformed(evt);
	      }
	    });
	    cp.add(button3);
	    
	    intoShoppingCart.setBounds(900, 90, 100, 25);
	    intoShoppingCart.setLabel("In den Warenkorb");
	    intoShoppingCart.addActionListener(new ActionListener() { 
		      public void actionPerformed(ActionEvent evt) { 
			        intoShoppingCart_ActionPerformed(evt);
			      }
			    });
	    cp.add(intoShoppingCart);
	    //canvas1.setBounds(184, 176, 236, 276);
	    //cp.add(canvas1);
	    //Rezept Liste	    
	    rezeptList.setBounds(150, 176, 250, 276);
	    cp.add(rezeptList);
	    for (int i = 0; i < Postman.getRecipies(gettrendid()).length; i++) {
		    rezeptList.add(Postman.getRecipies(gettrendid())[i]);
		}
	    //canvas2.setBounds(616, 168, 236, 276);
	    //cp.add(canvas2);
	    //Zutaten Liste
	    zutatenList.setBounds(450, 176, 250, 276);
	    cp.add(zutatenList);
	    for (int i = 0; i < Postman.getZutaten(gettrendid()).length; i++) {
			zutatenList.add(Postman.getZutaten(gettrendid())[i]);
		}
	    shoppingCartList.setBounds(750, 176, 250, 276);
	    cp.add(shoppingCartList);
	    kundBestList.setBounds(184, 176, 500, 276);
	    cp.add(kundBestList);
	    String kundenIDString = new String(Postman.getBestellungen(getkundenid()));
		kundBestList.add(kundenIDString);		
	    kundBestList.setVisible(false);
	    
	    textField1.setBounds(280, 96, 80, 20);
	    cp.add(textField1);
	    textField2.setBounds(200, 96, 80, 20);
	    cp.add(textField2);
	    
	    bestellen.setBounds(650, 96, 90, 20);
	    bestellen.setText("ID / Menge");
	    cp.add(bestellen);
	    shoppingCartID.setBounds(750, 96, 80, 20);
	    cp.add(shoppingCartID);
	    amount.setBounds(840, 96, 40, 20);
	    cp.add(amount);
	    
	    label1.setBounds(30, 96, 160, 20);
	    label1.setText("Auswahl RezeptID,KundenID");
	    cp.add(label1);
	    label2.setBounds(150, 152, 110, 20);
	    label2.setText("Rezepte");
	    cp.add(label2);
	    label3.setBounds(450, 152, 110, 20);
	    label3.setText("Zutaten");
	    cp.add(label3);
	    shoppingCart.setBounds(750, 152, 110, 20);
	    shoppingCart.setText("Warenkorb");
	    cp.add(shoppingCart);
	    kundBestLabel.setBounds(248, 144, 110, 20);
	    kundBestLabel.setText("getätigte Bestellungen");
	    kundBestLabel.setVisible(false);
	    cp.add(kundBestLabel);
	    canvas3.setBounds(152, 136, 785, 337);
	    cp.add(canvas3);
	    // Ende Komponenten
	    
	    setVisible(true);
	  } // end of public gui2
	  
	  // Anfang Methoden
	  
	  public static void main(String[] args) {
	    new GUI();
	  } // end of main
	  
	  public void button1_ActionPerformed(ActionEvent evt) {
		  zutatenList.setVisible(false);
		  rezeptList.setVisible(false);
		  label2.setVisible(false);
		  label3.setVisible(false);
		  kundBestList.setVisible(true);
		  kundBestLabel.setVisible(true);
		  button1.setVisible(false);
		  button4.setVisible(true);
	  } // end of button1_ActionPerformed

	  public void button2_ActionPerformed(ActionEvent evt) {
	    // TODO hier Quelltext einfügen
		  rezeptList.clear();
		    for (int i = 0; i < Postman.getRecipies(gettrendid()).length; i++) {
			    rezeptList.add(Postman.getRecipies(gettrendid())[i]);
			}
		    zutatenList.clear();
		    for (int i = 0; i < Postman.getZutaten(gettrendid()).length; i++) {
				zutatenList.add(Postman.getZutaten(gettrendid())[i]);
			}
		    kundBestList.clear();
		    String kundenIDString = new String(Postman.getBestellungen(getkundenid()));
			kundBestList.add(kundenIDString);
	  } // end of button2_ActionPerformed

	  int gettrendid() {
		  int trendid = choice1.getSelectedIndex();
		  return trendid;
	  }
	  int getkundenid() {
	//kunden Bestellungen Liste
	    String kundeIdString = textField1.getText();
	    int KundID = Integer.parseInt(kundeIdString);
	    return KundID;
	  }
	  
	  int getrezeptid() {
	//kunden Bestellungen Liste
	    String kundeIdString = textField2.getText();
	    int KundID = Integer.parseInt(kundeIdString);
	    return KundID;
	  }
	  
	  
	  ArrayList<String> rezeptidarray = new ArrayList<String>();
	  ArrayList<String> rezeptamountarray = new ArrayList<String>();
	  ArrayList<String> zutatidarray = new ArrayList<String>();
	  ArrayList<String> zutatamountarray = new ArrayList<String>();
	  
	  
	    
	  public void button3_ActionPerformed(ActionEvent evt) {
		  int[] shoppingrezeptid = new int[rezeptidarray.size()];
		  int[] shoppingrezeptamount = new int[rezeptidarray.size()];
		  int[] shoppingzutatid = new int[zutatidarray.size()];
		  int[] shoppingzutatamount = new int[zutatidarray.size()];
		  
		  for (int i = 0; i < rezeptidarray.size(); i++) {
			shoppingrezeptid[i] = Integer.valueOf(rezeptidarray.get(i));
		}
		  for (int i = 0; i < rezeptamountarray.size(); i++) {
				shoppingrezeptamount[i] = Integer.valueOf(rezeptamountarray.get(i));
			}
		  for (int i = 0; i < zutatidarray.size(); i++) {
				shoppingzutatid[i] = Integer.valueOf(zutatidarray.get(i));
			}
		  for (int i = 0; i < zutatamountarray.size(); i++) {
				shoppingzutatamount[i] = Integer.valueOf(zutatamountarray.get(i));
			}
		  Postman.doorder(getkundenid(), shoppingrezeptid, shoppingrezeptamount, shoppingzutatid, shoppingzutatamount);
	  }

	  public void button4_ActionPerformed(ActionEvent evt) {
		  zutatenList.setVisible(true);
		  rezeptList.setVisible(true);
		  label2.setVisible(true);
		  label3.setVisible(true);
		  kundBestList.setVisible(false);
		  kundBestLabel.setVisible(false);
		  button1.setVisible(true);
		  button4.setVisible(false);
		  } // end of button3_ActionPerformed
	  public void intoShoppingCart_ActionPerformed(ActionEvent evt) {
		  int choiceShopping = recipeOrIngredient.getSelectedIndex();
		  if (choiceShopping == 0) {
			  rezeptidarray.add(shoppingCartID.getText());
			  rezeptamountarray.add(amount.getText());
				  String shoppingline = "Rezept:"+rezeptamountarray.get(rezeptidarray.size()-1) + "* " + rezeptidarray.get(rezeptidarray.size()-1);
				  shoppingCartList.add(shoppingline);
		  }
		  if (choiceShopping == 1) {
			  zutatidarray.add(shoppingCartID.getText());
			  zutatamountarray.add(amount.getText());
				  String shoppingline = "Zutat:"+zutatamountarray.get(zutatidarray.size()-1) + "* " + zutatidarray.get(zutatidarray.size()-1);
				  shoppingCartList.add(shoppingline);
		  }
	  }

	  // Ende Methoden
}
