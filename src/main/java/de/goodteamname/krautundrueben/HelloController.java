package de.goodteamname.krautundrueben;

import java.sql.*;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class HelloController 
{

	public Connection connectToDB(String url) throws SQLException
	{
		// Verbindung zur Datenbank herstellen
		return DriverManager.getConnection(url);
	}
	
	public HelloController() 
	{
	
		try 
		{
			Class.forName(GlobalProperties.sqlDriver);

		} catch (ClassNotFoundException e) {
			System.err.println("Verbindung zum Datenbankserver nicht möglich!");
		}
	}
	
	
	@RequestMapping("/")
	public String index() 
	{
		return "<h1>Hallo Kraut und Rüben!</h1>";
	}
	
	@RequestMapping("/phil")
	public String phil()
	{
		return "<h1>Hallo von Philipp</h1>";
	}
	
	
	@GetMapping("/test")
	public String test()
	{
		String returnString = "";
		String url = GlobalProperties.URL();
		
		try (Connection c = connectToDB(url))
		{
	
			Statement s = c.createStatement();
			s.execute("SELECT * FROM rezept");
			ResultSet rSet = s.getResultSet();
			
			while (rSet.next()) 
			{
				returnString += rSet.getString("REZEPTNAME") + rSet.getInt("REZEPTNR") + "\n";
			}
			c.close();
			
		} catch (SQLException e) {
			System.err.println("Statement konnte nicht erstellt oder ausgeführt werden.");
			//e.printStackTrace();
		}
		
		return returnString;
	}
}
