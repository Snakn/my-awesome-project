package de.goodteamname.krautundrueben;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

public class PropertiesFileLoader 
{
	private PropertiesFileLoader() {
		// TODO Auto-generated constructor stub
	}
	
	public static Properties loadProperties() throws InvalidPropertiesFormatException, IOException
	{
		String filePath = "properties.xml";
		File file = new File(filePath);
		FileInputStream is = new FileInputStream(file);
		Properties props = new Properties();
		props.loadFromXML(is);
		is.close();
		
		Enumeration<Object> enumKeys = props.keys();
		while (enumKeys.hasMoreElements()) 
		{
			String key = (String) enumKeys.nextElement();
			String val = (String) props.getProperty(key);
			System.out.println(key + ": " + val);
		}
		
		return props;		
	}


}
