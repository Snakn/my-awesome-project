package de.goodteamname.krautundrueben.model;

public class ErnaehrungsTrend implements DataModel
{

	private String trendName;
	private int trendID;

	public ErnaehrungsTrend(String trendName, int trendID) {
		super();
		this.trendName = trendName;
		this.trendID = trendID;
	}
	
	
	public String getTrendName() {
		return trendName;
	}

	public void setTrendName(String trendName) {
		this.trendName = trendName;
	}

	public int getTrendID() {
		return trendID;
	}

	public void setTrendID(int trendID) {
		this.trendID = trendID;
	}
}
