package de.goodteamname.krautundrueben.model;

public class Rezept implements DataModel
{

	private String rezeptName;
	private int rezeptNummer;
	
	public Rezept(String name, int num) 
	{
		rezeptName = name;
		rezeptNummer = num;
	}

	//----------------------- Getters --------------------------------
	public String getRezeptName() 
	{
		return rezeptName;
	}
	
	public int getRezeptNummer() {
		return rezeptNummer;
	}
	
	// ------------------------- Setters ------------------------------
	public void setRezeptName(String rezeptName) 
	{
		this.rezeptName = rezeptName;
	}
	
	
	public void setRezeptNummer(int rezeptNummer) 
	{
		this.rezeptNummer = rezeptNummer;
	}
	
}
