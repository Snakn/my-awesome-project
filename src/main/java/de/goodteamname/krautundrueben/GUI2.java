package de.goodteamname.krautundrueben;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

import de.goodteamname.krautundrueben.utility.*;

import java.util.InvalidPropertiesFormatException;
import java.util.LinkedList;
import java.util.List;

import javax.swing.SpringLayout;

import org.apache.commons.io.FileUtils;



/**
 *
 * Beschreibung
 *
 * @version 1.0 vom 18.10.2020
 * @author 
 */

public class GUI2 extends Frame {
  // Anfang Attribute
  private Button button1 = new Button();
  private Button button2 = new Button();
  private Button button3 = new Button();
  private Button saveprop = new Button();
  private Button properties = new Button();
  private TextField urlText = new TextField();
  private NumberField numberPort = new NumberField();
  private Label urlLabel= new Label();
  private Label portLabel = new Label();
  private NumberField numberField1 = new NumberField();
  private Label label1 = new Label();
  private Choice choice1 = new Choice();
  private Label label2 = new Label();
  private Label label3 = new Label();
  private Label totalpriceLabel = new Label();
  private Label customerexists = new Label();

  private ScrollPane panel1 = new ScrollPane(ScrollPane.SCROLLBARS_AS_NEEDED);
  private ScrollPane panel2 = new ScrollPane(ScrollPane.SCROLLBARS_AS_NEEDED);
  private ScrollPane panel3 = new ScrollPane(ScrollPane.SCROLLBARS_AS_NEEDED);
  int port ;
  String url ;
  private List<NumberField> rezepte;
  private List<NumberField> zutaten;
  String[] recepiesS;
  String[] zutatenS;
  private Postman posty;
  // Ende Attribute    
  
  public GUI2() throws InvalidPropertiesFormatException, IOException { 
    // Frame-Initialisierung
    super();
    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent evt) { dispose(); }
    });
    int frameWidth = 899; 
    int frameHeight = 724;
    setSize(frameWidth, frameHeight);
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    int x = (d.width - getSize().width) / 2;
    int y = (d.height - getSize().height) / 2;
    setLocation(x, y);
    setTitle("GUI2");
    setResizable(false);
    Panel cp = new Panel(null);
    add(cp);
    // Anfang Komponenten
    
    button1.setBounds(40, 624, 91, 33);
    button1.setLabel("Bestellungen");
    button1.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        button1_ActionPerformed(evt);
      }
    });
    cp.add(button1);
    button2.setBounds(752, 624, 83, 33);
    button2.setLabel("bestellen");
    button2.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        button2_ActionPerformed(evt);
      }
    });
    cp.add(button2);
    saveprop.setBounds(752, 624, 83, 33);
    saveprop.setLabel("Speichern");
    saveprop.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
    	  try {
			saveprop_ActionPerformed(evt);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
      }
    });
    cp.add(saveprop);
    button3.setBounds(648, 624, 83, 33);
    button3.setLabel("Zurück");
    button3.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        button3_ActionPerformed(evt);
      }
    });
    cp.add(button3);
    properties.setBounds(350, 624, 83, 33);
    properties.setLabel("Einstellungen");
    properties.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
    	  properties_ActionPerformed(evt);
      }
    });
    cp.add(properties);
    numberField1.addTextListener(new TextListener() {
   	 public void textValueChanged(TextEvent e) {
			 customerEdited(e);
		  }
   });
    numberField1.setBounds(144, 56, 123, 20);
    numberField1.setText("");
    cp.add(numberField1);
    numberPort.setBounds(144, 56, 123, 20);
    cp.add(numberPort);
    customerexists.setBounds(24,25,110,20);
    cp.add(customerexists);
    portLabel.setBounds(24, 56, 110, 20);
    portLabel.setText("Port");
    cp.add(portLabel);
   urlText.setBounds(144, 156, 123, 20);
   cp.add(urlText);
   urlLabel.setBounds(24, 156, 110, 20);
   urlLabel.setText("IP");
   cp.add(urlLabel);
    label1.setBounds(24, 56, 110, 20);
    label1.setText("KundenID");
    cp.add(label1);
    choice1.setBounds(312, 56, 150, 20);
    choice1.addItemListener(new ItemListener() {
    	public void itemStateChanged(ItemEvent e) {
    		choice1_diffItemSelected(e);
    	};
    });
    cp.add(choice1);
    label2.setBounds(176, 128, 110, 20);
    label2.setText("Rezepte");
    cp.add(label2);
    label3.setBounds(616, 120, 110, 20);
    label3.setText("Zutaten");
    cp.add(label3);
    totalpriceLabel.setBounds(648, 624, 83, 33);
    totalpriceLabel.setText("0,00€");
    cp.add(totalpriceLabel);
    panel1.setBounds(96, 176, 400, 400);
    cp.add(panel1);
    panel2.setBounds(528, 176, 300, 400);
    cp.add(panel2);
    panel3.setBounds(56, 152, 793, 409);
    cp.add(panel3);
    // Ende Komponenten
    GlobalProperties.setGlobalProperties();
    port = GlobalProperties.httpport;
    url = GlobalProperties.dbHostName;
    setVisible(true);
    screen2(false);
    screen3(false);
    posty = new Postman(url, port);
    numberField1.setInt(2001);
    updatetrends();
    choice1_diffItemSelected(null);
    recalcTotalPrice();
    numberPort.setInt(port);
    urlText.setText(url);
  } // end of public GUI2
  
  public void updatetrends() {
	  String[] trends = posty.trends();
	    for(int i = 0; i < trends.length; i++) {
	    	choice1.add(trends[i]);
	    }
  }
  public void updateScreen1() {
	  panel1.removeAll();
	  panel2.removeAll();
	  choice1.removeAll();
	  updatetrends();
	  choice1_diffItemSelected(null);
	  customerEdited(null);
  }
  public void saveprop_ActionPerformed(ActionEvent evt) throws IOException {
	  port = numberPort.getInt();
	  url = urlText.getText();
	  File file = new File("properties.xml");
	  List<String> lines = FileUtils.readLines(file, Charset.forName("UTF-8"));
	  for(int i = 0; i < lines.size(); i ++) {
		  if(lines.get(i).contains("<entry key=\"httpport\">")) {
			  lines.set(i, "<entry key=\"httpport\">"+port+"</entry>"+System.lineSeparator());
		  }
		  if(lines.get(i).contains("<entry key=\"db_host\">")) {
			  lines.set(i, "<entry key=\"db_host\">"+url+"</entry>"+System.lineSeparator());
		  }
	  }
	 FileUtils.writeLines(file, lines);
	 posty = new Postman(url,port);
	 updateScreen1();	
  }
  public void customerEdited(TextEvent e) {
	  int id = 0 ;
	  try {
	  id = numberField1.getInt();
	  } catch(Exception ec) {
		  customerexists.setText("Existiert nicht");
		  customerexists.setBackground(Color.red);
		  return;
	  }
	  if(posty.getCustomer(id)) {
		  customerexists.setText("");
		  customerexists.setBackground(null);
		  return;
	  }else {
		  customerexists.setText("Existiert nicht");
		  customerexists.setBackground(Color.red);
		  return;
	  }
  }
  public void updateScreen2() {
	  byte[] bestellungenb = posty.getBestellungen(numberField1.getInt());
	  String bestellungens = new String(bestellungenb);
	  String[] bstls = bestellungens.split(System.lineSeparator());
	  panel3.removeAll();
	  TextArea ta = new TextArea();
	  panel3.add(ta);
	  ta.setPreferredSize(new Dimension(800,18*bstls.length));
	  ta.setText(bestellungens);
	  ta.setVisible(true);
	  ta.setEnabled(false);
	  panel3.doLayout();
	  panel3.setVisible(true);
  }
  // Anfang Methoden
  public void screen1(boolean visible) {
	  button1.setVisible(visible);
	  button2.setVisible(visible);
	  panel1.setVisible(visible);
	  panel2.setVisible(visible);
	  label3.setVisible(visible);
	  label2.setVisible(visible);
	  label1.setVisible(visible);
	  numberField1.setVisible(visible);
	  choice1.setVisible(visible);
	  totalpriceLabel.setVisible(visible);
	  properties.setVisible(visible);
	  customerexists.setVisible(visible);
  }
  public void screen2(boolean visible) {
	  button3.setVisible(visible);
	  panel3.setVisible(visible);
  }
  public static void main(String[] args) throws InvalidPropertiesFormatException, IOException {
    new GUI2();
  } // end of main
  
  public void button1_ActionPerformed(ActionEvent evt) {
    // Switch from main screen to Bestellungs screen
	updateScreen2();
    screen2(true);
    screen1(false);
  } // end of button1_ActionPerformed

  public void choice1_diffItemSelected(ItemEvent e) {
	  int trendid = choice1.getSelectedIndex();
	  String[] recepies = posty.getRecipies(trendid);
	  String[] ingredients = posty.getZutaten(trendid);
	  rezepte = new LinkedList<NumberField>();
	  zutaten = new LinkedList<NumberField>();
	  zutatenS = ingredients;
	  recepiesS = recepies;
	  updateRecepies(recepies);
	  updateZutaten(ingredients);
	  recalcTotalPrice();
  }
  public void updateRecepies(String[] recepies) {
	  panel1.removeAll();
	  if(recepies== null) {
		  return;
	  }
	  Panel pane = new Panel();
	  pane.setBounds(0, 0, 300, 100*recepies.length);
	  pane.setPreferredSize(new Dimension(350,50*recepies.length));
	  int lowbound = 0;
	  for(int i = 0 ; i < recepies.length; i++) {
		  addNumberFieldComp(recepies[i],pane,rezepte,lowbound,350);
		  lowbound+=50;
	  }
	  panel1.add(pane);
	  panel1.doLayout();
	  pane.setVisible(true);
  }
  public void addNumberFieldComp(String name,Panel panel, List<NumberField> thing,int lowbound,int labsize) {
	  Label lab = new Label();
	  lab.setText(name);
	  lab.setSize(250, 100);
	  lab.setBounds(0, lowbound, labsize, 25);
	  lab.setVisible(true);
	  panel.add(lab);
	  NumberField num = new NumberField();
	  num.addTextListener(new TextListener() {
		  public void textValueChanged(TextEvent e) {
			  numEdited(e);
		  }
	  });
	  num.setInt(0);
	  num.setSize(50, 100);
	  num.setVisible(true);
	  panel.add(num);
	  num.setBounds(labsize, lowbound, 25, 25);
	  thing.add(num);
	  
  }
  public void numEdited(TextEvent e) {
	  recalcTotalPrice();
	  
  }
  public void screen3(boolean visible) {
	  numberPort.setVisible(visible);
	  urlText.setVisible(visible);
	  button3.setVisible(visible);
	  urlLabel.setVisible(visible);
	  portLabel.setVisible(visible);
	  saveprop.setVisible(visible);
  }
  public void recalcTotalPrice() {
	  double price = 0.00;
	  int amount;
	  for(int i = 0 ; i < rezepte.size();i++) {
		  try {
		  amount = rezepte.get(i).getInt();
		  }catch(Exception e) {
			  amount = 0;
		  }
		  if(amount != 0) {
			  String[] priceone = recepiesS[i].split("Rezeptpreis :");
			  String priceonee = priceone[priceone.length-1];
			  price += amount * (Double.parseDouble(priceonee));
		  }
	  }
	  for(int i = 0 ; i < zutaten.size();i++) {
		  try {
		  amount = zutaten.get(i).getInt();
		  }catch(Exception e) {
			  amount = 0;
		  }
		  if(amount != 0) {
			  String[] priceone = zutatenS[i].split("Preis :");
			  String priceonee = priceone[priceone.length-1];
			  price += amount * (Double.parseDouble(priceonee));
		  }
	  }
	  totalpriceLabel.setText(String.format("%.2f", price)+"" +"€");
  }
  public void updateZutaten(String[] ingredients) {
	  panel2.removeAll();
	  if(ingredients== null) {
		  return;
	  }
	  Panel pane = new Panel();
	  pane.setBounds(0, 0, 300, 100*ingredients.length);
	  pane.setPreferredSize(new Dimension(250,50*ingredients.length));
	  int lowbound = 0;
	  for(int i = 0 ; i < ingredients.length; i++) {
		  addNumberFieldComp(ingredients[i],pane,zutaten,lowbound,250);
		  lowbound+=50;
	  }
	  panel2.add(pane);
	  panel2.doLayout();
	  pane.setVisible(true);
  }
  public void button2_ActionPerformed(ActionEvent evt) {
    // TODO hier Quelltext einf�gen
	  int[] rezeptamounts = new int[rezepte.size()];
	  int[] zutatenamounts = new int[zutaten.size()];
	  for(int i = 0 ; i < rezeptamounts.length; i++) {
		  rezeptamounts[i] = rezepte.get(i).getInt();
		  rezepte.get(i).setInt(0);
	  }
	  for(int j = 0 ; j < zutatenamounts.length; j++) {
		  zutatenamounts[j] = zutaten.get(j).getInt();
		  zutaten.get(j).setInt(0);
	  } 
    posty.doorder(numberField1.getInt(), posty.rezeptids, posty.zutatenids, rezeptamounts, zutatenamounts);
    recalcTotalPrice();
  } // end of button2_ActionPerformed

  public void button3_ActionPerformed(ActionEvent evt) {
    // TODO hier Quelltext einf�gen
    screen1(true);
    screen2(false);
    screen3(false);
  } // end of button3_ActionPerformed

  
  public void properties_ActionPerformed(ActionEvent evt) {
	 screen3(true);
	 screen1(false);
  }
  // Ende Methoden
} // end of class GUI2

