package de.goodteamname.krautundrueben;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

public class Postman {
	int[] rezeptids;
	int[] zutatenids;
	String url;
	int port;
	public Postman(String url,int port) {
		this.url = url;
		this.port = port;
	}
	
	
	String[] getRecipies(int trendid) {
		MultiValueMap<String,Object> map = new LinkedMultiValueMap<String,Object>();
		map.add("trendid", trendid);
		ResponseEntity<byte[]> r = sendarequest("/rezepttrend",map);
		if(r.getBody()== null) {
			return null;
		}
		String[] working = new String(r.getBody()).split(System.lineSeparator());
		String[] result = new String[working.length/2];
		rezeptids = new int[result.length];
		for(int i = 0; i < working.length;i+=2) {
			rezeptids[i/2] = Integer.parseInt(working[i]);
			result[i/2] = working[i+1];
		}
		return result;
	}
	
	boolean getCustomer(int kundenid) {
		boolean answer = false;
		MultiValueMap<String,Object> map = new LinkedMultiValueMap<String,Object>();
		map.add("kundenid", kundenid);
		ResponseEntity<byte[]> r = sendarequest("/KundeExistiert",map);
		if(r.getBody()!=null) {
			return true;
		}
		return answer;
	}
	
	String[] getZutaten(int trendid) {
		MultiValueMap<String,Object> map = new LinkedMultiValueMap<String,Object>();
		map.add("trendid", trendid);
		ResponseEntity<byte[]> r = sendarequest("/zutattrend",map);
		if(r.getBody()== null) {
			return null;
		}                                
		String[] working = new String(r.getBody()).split(System.lineSeparator());
		String[] result = new String[working.length/2];
		zutatenids = new int[result.length];
		for(int i = 0; i < working.length;i+=2) {
			zutatenids[i/2] = Integer.parseInt(working[i]);
			result[i/2] = working[i+1];
		}
		return result;
	}
	
	
	byte[] getBestellungen(int kundenid) {
		// will give all Bestellungen of a Customer
		MultiValueMap<String,Object> map = new LinkedMultiValueMap<String,Object>();
		map.add("kundenid", kundenid);
		ResponseEntity<byte[]> r = sendarequest("/Bestellungen",map);
		return r.getBody();
	}
	
	void doorder(int kundenid,int[] rezeptids,int[] zutatenids,int[] rezeptamount,int[] zutatenamount) {
		// makes an order for a customer
		MultiValueMap<String,String> map = new LinkedMultiValueMap<>();
		int[] kundenids =new int[1];
		kundenids[0] = kundenid;
		String kundenidsS = intToString(kundenids);
		String rezeptidsS = intToString(rezeptids);
		String zutatenidsS = intToString(zutatenids);
		String rezeptamountS = intToString(rezeptamount);
		String zutatenamountS = intToString(zutatenamount);
		map.add("kundenid", kundenidsS);
		map.add("RezeptIDs", rezeptidsS);
		map.add("ZutatenIDs", zutatenidsS);
		map.add("RezeptAmounts", rezeptamountS);
		map.add("ZutatenAmounts", zutatenamountS);
		sendarequest2("/MultipleBestellungen",map);
	}

	public String intToString(int[] ia) {
		String answer = "";
		for(int i = 0 ; i < ia.length;i++) {
			if((i+1)<ia.length) {
			answer += ia[i]+",";}else {
				answer+= ia[i];
			}
		}
		return answer;
	}
	
	public ResponseEntity<byte[]> sendarequest(String option,MultiValueMap<String,Object> map){
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);

		HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<MultiValueMap<String, Object>>(map, headers);
		return  restTemplate.postForEntity( "http://"+url+":"+port+option, request , byte[].class );
	}
	
	
	public ResponseEntity<byte[]> sendarequest2(String option,MultiValueMap<String,String> map){
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);

		HttpEntity<MultiValueMap<String,String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
		return  restTemplate.postForEntity( "http://"+url+":"+port+option, request , byte[].class );
	}
	
	
	int[] getRezeptids() {
		return rezeptids;
	}
	int[] getZutatenids() {
		return zutatenids;
	}
	
	public String[] trends() {
		MultiValueMap<String,Object> map = new LinkedMultiValueMap<String,Object>();
		map.add("trendid", 0);
		ResponseEntity<byte[]> response = sendarequest("/gettrends",map);
		String trend = new String("Alle"+System.lineSeparator()+(new String(response.getBody())));	
		String[] trends = trend.split(System.lineSeparator());
		return trends;
	}
}
