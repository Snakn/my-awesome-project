package de.goodteamname.krautundrueben;

import de.goodteamname.krautundrueben.model.*;
import java.sql.*;
import java.util.ArrayList;

public class DbController 
{
	public Connection connectToDB(String url) throws SQLException
	{
		return DriverManager.getConnection(url);
	}
	
	
	public ArrayList<DataModel> getData(String type, int trendID) throws SQLException
	{
		switch (type.toLowerCase()) {
		case "rezept":
			return getRezept(trendID);
		case "trend":
			return getTrends();
		}
		return null;
	}
	
	
	private ArrayList<DataModel> getRezept(int trendID) throws SQLException
	{
		ArrayList<DataModel> dm = new ArrayList<DataModel>();
		Connection c = connectToDB(GlobalProperties.URL());
		Statement s = c.createStatement();
		String sql = "SELECT * FROM rezept";
		if(trendID != 0)
		{
			sql = "SELECT * FROM rezept AS ori\r\n"
					+ "WHERE \r\n"
					+ "(SELECT COUNT(rezept.REZEPTNR) FROM rezept\r\n"
					+ "LEFT JOIN rezeptzutat\r\n"
					+ "ON rezeptzutat.REZEPTNR = rezept.REZEPTNR\r\n"
					+ "WHERE rezept.REZEPTNR = ori.REZEPTNR)\r\n"
					+ "=\r\n"
					+ "(SELECT COUNT(REZEPTZUTAT.Zutatennr)\r\n"
					+ "FROM TRENDZUORDNUNG\r\n"
					+ "right JOIN rezeptzutat\r\n"
					+ "ON trendzuordnung.ZUTATENNR = REZEPTZUTAT.ZUTATENNR\r\n"
					+ "INNER JOIN rezept \r\n"
					+ "ON REZEPTZUTAT.REZEPTNR = rezept.REZEPTNR WHERE trendzuordnung.trendnr = " + trendID + " AND rezept.REZEPTNR = ori.REZEPTNR);";
		}
		s.execute(sql);
		ResultSet resultSet = s.getResultSet();
		
		while(resultSet.next())
		{
			Rezept r = new Rezept(resultSet.getString("REZEPTNAME"), resultSet.getInt("REZEPTNR"));
			dm.add(r);
		}
		c.close();
		return dm;
	}
	
	private ArrayList<DataModel> getTrends() throws SQLException
	{
		ArrayList<DataModel> dm = new ArrayList<DataModel>();
		Connection c = connectToDB(GlobalProperties.URL());
		Statement s = c.createStatement();
		String sql = "SELECT * FROM ernaehrungstrend";
		s.execute(sql);
		ResultSet resultSet = s.getResultSet();
		dm.add(new ErnaehrungsTrend("Alle", 0));
		while(resultSet.next())
		{
			ErnaehrungsTrend r = new ErnaehrungsTrend(resultSet.getString("ERNAEHRUNGSNAME"), resultSet.getInt("ERNAEHRUNGSID"));
			dm.add(r);
		}
		c.close();
		return dm;
	}
}
