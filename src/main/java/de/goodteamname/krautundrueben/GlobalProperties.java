package de.goodteamname.krautundrueben;

import java.io.IOException;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

public class GlobalProperties 
{
	private GlobalProperties() 
	{
		// rein statische Klasse
	}
	
	public static String dbHostName;
	public static String sqlDriver;
	public static String dbName;
	public static String dbUser;
	public static int port;
	public static int httpport;
	
	public static void setGlobalProperties() throws InvalidPropertiesFormatException, IOException 
	{
		Properties properties = PropertiesFileLoader.loadProperties();
		httpport = Integer.parseInt(properties.getProperty("httpport"));
		dbHostName = properties.getProperty("db_host");
		dbName = properties.getProperty("db_name");
		dbUser = properties.getProperty("db_user");
		sqlDriver = properties.getProperty("sql_driver");
		port = Integer.parseInt(properties.getProperty("port"));
	}
	
	public static String URL()
	{
		return "jdbc:mysql://" + dbHostName + "/" + dbName + "?" + "user=" + dbUser;
	}
}
