package de.goodteamname.krautundrueben;
import de.goodteamname.krautundrueben.model.*;

import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class JspController 
{

	
	@RequestMapping(value= {"/","/index"}, method = RequestMethod.GET)
	public String index(Model model) 
	{
		String message = "Hallo Kraut und Rüben!";
		model.addAttribute("message", message);
		return "index";
	}
	
	@GetMapping("/recipies")
	public String viewRezeptModel(Model model, @RequestParam(name="trend", defaultValue="0") int trendID) 
	{
		System.out.println("Empfange: " + trendID);
		ArrayList<DataModel> rezeptList = new ArrayList<DataModel>();
		ArrayList<DataModel> trendList = new ArrayList<DataModel>();
		
		DbController dbController = new DbController();
		// Rezepte holen
		
		try 
		{
			rezeptList = dbController.getData("rezept", trendID);
			trendList = dbController.getData("trend", 0);
				
		}
		
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		model.addAttribute("rezepte", rezeptList);
		model.addAttribute("trends", trendList);
		
		return "/index";
	}
}
