package de.goodteamname.krautundrueben;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.sql.*;

import java.util.ArrayList;
//import org.apache.commons.lang3.ArrayUtils;

@RestController
public class KrautControll {
	String connectTo = "jdbc:mysql://localhost/krautundrueben?user=root";
	
	public Connection connect() {
		Connection c = null;
		try {
		Class.forName("com.mysql.jdbc.Driver");
		c = DriverManager.getConnection(connectTo);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return c;
	}
	
	@PostMapping(path="/Rezepte")
	public ResponseEntity<byte[]> order () throws SQLException {
		Connection c = connect();
		Statement s = c.createStatement();
		
		String sql = "SELECT * FROM REZEPT";
		
		s.execute(sql);
		
		ResultSet r = s.getResultSet();
		ArrayList<byte[]> answerlist  = new ArrayList<byte[]>();
		while(r.next()) {
			answerlist.add((r.getInt("RezeptNr")+System.lineSeparator()+"").getBytes());
			if(r.getString("Preis")!= null){
			answerlist.add(("Rezeptname :"+r.getString("Rezeptname")+" Rezeptpreis :"+r.getString("Preis")+System.lineSeparator()).getBytes());
			}else {
			answerlist.add(("Rezeptname :"+r.getString("Rezeptname")+" Rezeptpreis : "+System.lineSeparator()).getBytes());
			}
			
		}
		byte[] a =delistify(answerlist);
		c.close();
		return makeMeAnOutput(a);
	}
	
	@PostMapping(path="/preisupdate")
	public ResponseEntity<byte[]> preisupdate() throws SQLException{
		Connection c = connect();
		Statement s = c.createStatement();
		String sql = "SELECT * FROM REZEPT;";
		s.execute(sql);
		ResultSet r = s.getResultSet();
		while(r.next()) {
			Statement temps = c.createStatement();
			String id = r.getString("Rezeptnr");
			String tempsql = "UPDATE rezept SET PREIS = (\r\n"
					+ "SELECT SUM(NETTOPREIS * MENGE) FROM REZEPTZUTAT \r\n"
					+ "LEFT JOIN Zutat \r\n"
					+ "ON REZEPTZUTAT.ZUTATENNR=ZUTAT.ZUTATENNR \r\n"
					+ "WHERE REZEPTNR="+id+")\r\n"
					+ "WHERE REZEPTNR="+id+";";
			temps.execute(tempsql);
			Statement temps2 = c.createStatement();
			id = r.getString("Rezeptnr");
			tempsql = "UPDATE REZEPT SET KALORIEN = (\r\n"
					+ "SELECT  SUM(KALORIEN * MENGE) FROM REZEPTZUTAT R \r\n"
					+ "LEFT JOIN ZUTAT Z \r\n"
					+ "ON R.ZUTATENNR = Z.ZUTATENNR\r\n"
					+ "WHERE REZEPTNR="+id+")\r\n"
					+ "WHERE REZEPTNR="+id+";";
			temps2.execute(tempsql);
		}
		byte[] a = "done!".getBytes();
		c.close();
		return makeMeAnOutput(a);
	}
	@PostMapping(path="/gettrends")
	public ResponseEntity<byte[]> gettrends() throws SQLException{
		Connection c = connect();
		Statement s = c.createStatement();
		String sql= "SELECT ernaehrungsname FROM ernaehrungstrend ;";
		s.execute(sql);
		
		ResultSet r = s.getResultSet();
		ArrayList<byte[]> answerlist  = new ArrayList<byte[]>();
		while(r.next()) {
		
			answerlist.add((r.getString("ernaehrungsname")+System.lineSeparator()).getBytes());
			
		}
		byte[] a =delistify(answerlist);
		c.close();
		return makeMeAnOutput(a);
	}
	
	@PostMapping(path="/rezepttrend")
	public ResponseEntity<byte[]> rezepttrend(@RequestParam("trendid") int trendid) throws SQLException{
		Connection c = connect();
		preisupdate();
		if(trendid == 0) {
			return order();
		}
		Statement s = c.createStatement();
		String sql= "SELECT * FROM rezept AS ori\r\n"
				+ "WHERE \r\n"
				+ "(SELECT COUNT(rezept.REZEPTNR) FROM rezept\r\n"
				+ "LEFT JOIN rezeptzutat\r\n"
				+ "ON rezeptzutat.REZEPTNR = rezept.REZEPTNR\r\n"
				+ "WHERE rezept.REZEPTNR = ori.REZEPTNR)\r\n"
				+ "=\r\n"
				+ "(SELECT COUNT(REZEPTZUTAT.Zutatennr)\r\n"
				+ "FROM TRENDZUORDNUNG\r\n"
				+ "right JOIN rezeptzutat\r\n"
				+ "ON trendzuordnung.ZUTATENNR = REZEPTZUTAT.ZUTATENNR\r\n"
				+ "INNER JOIN rezept \r\n"
				+ "ON REZEPTZUTAT.REZEPTNR = rezept.REZEPTNR WHERE trendzuordnung.trendnr = "+trendid+" AND rezept.REZEPTNR = ori.REZEPTNR);";
		s.execute(sql);
		
		ResultSet r = s.getResultSet();
		ArrayList<byte[]> answerlist  = new ArrayList<byte[]>();
		while(r.next()) {
			if(r.getString("Preis")!= null){
			answerlist.add((r.getInt("RezeptNr")+System.lineSeparator()+"").getBytes());
			answerlist.add(("Rezeptname :"+r.getString("Rezeptname")+" Rezeptpreis :"+r.getString("Preis")+System.lineSeparator()).getBytes());
			}else {
				answerlist.add(("Rezeptname :"+r.getString("Rezeptname")+" Rezeptpreis : "+System.lineSeparator()).getBytes());
			}
		}
		byte[] a =delistify(answerlist);
		c.close();
		return makeMeAnOutput(a);
		
	}
	
	@PostMapping(path="/zutattrend")
	public ResponseEntity<byte[]> zutattrend(@RequestParam("trendid") int trendid) throws SQLException{
		Connection c = connect();
		if(trendid == 0) {
			return zutaten();
		}
		Statement s = c.createStatement();
		String sql= "SELECT * \r\n"
				+ "FROM TRENDZUORDNUNG\r\n"
				+ "right JOIN zutat\r\n"
				+ "ON trendzuordnung.ZUTATENNR = ZUTAT.ZUTATENNR\r\n"
				+ "WHERE trendzuordnung.trendnr = "+trendid+";";
		s.execute(sql);
		
		ResultSet r = s.getResultSet();
		ArrayList<byte[]> answerlist  = new ArrayList<byte[]>();
		while(r.next()) {
			answerlist.add((r.getInt("ZutatenNr")+System.lineSeparator()+"").getBytes());
			if(r.getString("NettoPreis")!= null){
			answerlist.add(("Bezeichnung :"+r.getString("Bezeichnung")+" Preis :"+r.getString("NettoPreis")+System.lineSeparator()).getBytes());
			}else {
			answerlist.add(("Bezeichnung :"+r.getString("Bezeichnung")+" Preis : "+System.lineSeparator()).getBytes());
			}
		}
		byte[] a =delistify(answerlist);
		c.close();
		return makeMeAnOutput(a);
		
	}
	
	public ResponseEntity<byte[]> zutaten() throws SQLException{
		Connection c = connect();
		Statement s = c.createStatement();
		
		String sql = "SELECT * FROM Zutat";
		
		s.execute(sql);
		
		ResultSet r = s.getResultSet();
		ArrayList<byte[]> answerlist  = new ArrayList<byte[]>();
		while(r.next()) {
			answerlist.add((r.getInt("ZutatenNr")+System.lineSeparator()+"").getBytes());
			if(r.getString("NettoPreis")!= null){
			answerlist.add(("Bezeichnung :"+r.getString("Bezeichnung")+" Preis :"+r.getString("NettoPreis")+System.lineSeparator()).getBytes());
			}else {
			answerlist.add(("Bezeichnung :"+r.getString("Bezeichnung")+" Preis : "+System.lineSeparator()).getBytes());
			}
			
		}
		byte[] a =delistify(answerlist);
		c.close();
		return makeMeAnOutput(a);
	}
	
	
	@PostMapping(path="/trendselect")
	public ResponseEntity<byte[]> trendselect(@RequestParam("trendid") int trendid) throws SQLException{
		Connection c = connect();
		Statement s = c.createStatement();
		String sql= "SELECT * FROM rezept AS ori\r\n"
				+ "WHERE \r\n"
				+ "(SELECT COUNT(rezept.REZEPTNR) FROM rezept\r\n"
				+ "LEFT JOIN rezeptzutat\r\n"
				+ "ON rezeptzutat.REZEPTNR = rezept.REZEPTNR\r\n"
				+ "WHERE rezept.REZEPTNR = ori.REZEPTNR)\r\n"
				+ "=\r\n"
				+ "(SELECT COUNT(REZEPTZUTAT.Zutatennr)\r\n"
				+ "FROM TRENDZUORDNUNG\r\n"
				+ "right JOIN rezeptzutat\r\n"
				+ "ON trendzuordnung.ZUTATENNR = REZEPTZUTAT.ZUTATENNR\r\n"
				+ "INNER JOIN rezept \r\n"
				+ "ON REZEPTZUTAT.REZEPTNR = rezept.REZEPTNR WHERE trendzuordnung.trendnr = "+trendid+" AND rezept.REZEPTNR = ori.REZEPTNR);";
		s.execute(sql);
		
		ResultSet r = s.getResultSet();
		ArrayList<byte[]> answerlist  = new ArrayList<byte[]>();
		while(r.next()) {
			if(r.getString("Preis")!= null){
			answerlist.add(("Rezeptname :"+r.getString("Rezeptname")+" Rezeptpreis :"+r.getString("Preis")+System.lineSeparator()).getBytes());
			}else {
				answerlist.add(("Rezeptname :"+r.getString("Rezeptname")+" Rezeptpreis : "+System.lineSeparator()).getBytes());
			}
		}
		byte[] a =delistify(answerlist);
		c.close();
		return makeMeAnOutput(a);
	}
	
	@PostMapping(path="/bestellRezept")
	public ResponseEntity<byte[]> bestellRezept(@RequestParam("RezeptID") int rezeptid,@RequestParam("KundenID") int kundenid) throws SQLException{
		Connection c = connect();
		int bestellid = orderinit(kundenid,c);
		Statement s = c.createStatement();
		String sql = "UPDATE bestellung\r\n"
				+ "    SET ADRESSEID =\r\n"
				+ "        (SELECT ADRESSEID FROM kunde \r\n"
				+ "            WHERE KUNDENNR = "+kundenid+")\r\n"
				+ "        WHERE BESTELLNR = "+bestellid+";";
		s.execute(sql);
		s = c.createStatement();
		sql = "UPDATE bestellung\r\n"
				+ "    SET RECHNUNGSBETRAG =\r\n"
				+ "        (\r\n"
				+ "    SELECT SUM(NETTOPREIS * MENGE) FROM REZEPTZUTAT \r\n"
				+ "    LEFT JOIN Zutat \r\n"
				+ "    ON REZEPTZUTAT.ZUTATENNR=ZUTAT.ZUTATENNR \r\n"
				+ "    WHERE REZEPTNR="+rezeptid+")\r\n"
				+ "    WHERE BESTELLNR = "+bestellid+";";
		s.execute(sql);
		s = c.createStatement();
		sql = "INSERT INTO bestellungzutat (BESTELLNR, ZUTATENNR, MENGE)\r\n"
				+ "    SELECT "+bestellid+",rzt.ZUTATENNR,rzt.MENGE\r\n"
				+ "    FROM rezeptzutat rzt \r\n"
				+ "    WHERE REZEPTNR = "+rezeptid+";";
		s.execute(sql);
		byte[] a = "done!".getBytes();
		c.close();
		return makeMeAnOutput(a);
	}
	
	public int orderinit(int kundenid,Connection c) throws SQLException {
		Statement s = c.createStatement();
		String sql = "INSERT INTO \r\n"
				+ "    bestellung (KUNDENNR,BESTELLDATUM)\r\n"
				+ "    VALUES\r\n"
				+ "    ("+kundenid+",current_timestamp);";
		s.execute(sql);
		s = c.createStatement();
		sql = "Select Bestellnr From bestellung Where Bestellnr = LAST_INSERT_ID();";
		s.execute(sql);
		ResultSet r = s.getResultSet();
		r.next();
		return r.getInt("BestellNr");
	}
	
	public int[] intToString(String s) {
		String[] s2 = s.split(",");
		int[] answer = new int[s2.length];
		for(int i = 0 ; i < s2.length;i++) {
			answer[i] = Integer.parseInt(s2[i]);
		}
		return answer;
	}
	
	@PostMapping(path="/MultipleBestellungen")
	public ResponseEntity<byte[]> multiorder(@RequestParam("kundenid") int kundenid,@RequestParam("RezeptIDs") String rezeptidsS,@RequestParam("ZutatenIDs") String zutatenidsS,@RequestParam("ZutatenAmounts") String zutatenamountsS,@RequestParam("RezeptAmounts") String rezeptamountsS) throws SQLException{
		int[] rezeptids = intToString(rezeptidsS);
		int[] zutatenids = intToString(zutatenidsS);
		int[] zutatenamounts = intToString(zutatenamountsS);
		int[] rezeptamounts = intToString(rezeptamountsS);
		boolean anything = false;
		for(int i = 0; i < zutatenamounts.length;i++) {
			if(anything) {
				break;
			}
			if(zutatenamounts[i] != 0) {
				anything = true;
			}
		}
		for(int i = 0; i < rezeptamounts.length;i++) {
			if(anything) {
				break;
			}
			if(rezeptamounts[i] != 0) {
				anything = true;
			}
		}
		if(!anything) {
			return null;
		}
		Connection c = connect();
		int bestellid = orderinit(kundenid,c);
		Statement s = c.createStatement();
		String sql = "UPDATE bestellung\r\n"
				+ "    SET ADRESSEID =\r\n"
				+ "        (SELECT ADRESSEID FROM kunde \r\n"
				+ "            WHERE KUNDENNR = "+kundenid+")\r\n"
				+ "        WHERE BESTELLNR = "+bestellid+";";
		s.execute(sql);
		BigDecimal rechnungsbetrag = new BigDecimal("0.0");
		preisupdate();
		for(int i = 0; i< rezeptids.length;i++) {
			if(rezeptamounts[i]!=0) {
			s = c.createStatement();
			sql = "Select Preis From Rezept Where RezeptNR="+rezeptids[i]+";";
			s.execute(sql);
			ResultSet r = s.getResultSet();
			r.next();
			rechnungsbetrag =rechnungsbetrag.add( r.getBigDecimal("Preis").multiply(new BigDecimal(rezeptamounts[i])));
			}
		}
		for (int i = 0;i < zutatenids.length;i++) {
			if(zutatenamounts[i]!=0) {
			s = c.createStatement();
			sql = "Select NettoPreis From Zutat Where ZutatenNR="+zutatenids[i]+";";
			s.execute(sql);
			ResultSet r = s.getResultSet();
			r.next();
			rechnungsbetrag =rechnungsbetrag.add( r.getBigDecimal("NettoPreis").multiply(new BigDecimal(zutatenamounts[i])));
			}
		}
		ArrayList<Integer> newzutatenids = new ArrayList<Integer>();
		ArrayList<Integer> newzutatenamounts = new ArrayList<Integer>();
		for (int i = 0;i < zutatenids.length;i++) {
			if(zutatenamounts[i]!=0) {
			newzutatenids.add(zutatenids[i]);
			newzutatenamounts.add(zutatenamounts[i]);
			}
		}
		for(int i = 0; i < rezeptids.length;i++) {
			if(rezeptamounts[i]!=0) {
			s = c.createStatement();
			sql = "Select * From RezeptZutat Where rezeptNR="+rezeptids[i]+";";
			s.execute(sql);
			ResultSet r = s.getResultSet();
			while(r.next()) {
				int zutatenid = r.getInt("Zutatennr");
				int zutatenmenge = r.getInt("Menge");
				zutatenmenge = zutatenmenge * rezeptamounts[i];
				if(newzutatenids.contains(zutatenid)) {
					int index = newzutatenids.indexOf(zutatenid);
					newzutatenamounts.set(index, newzutatenamounts.get(index)+zutatenmenge);
				}else {
					newzutatenids.add(zutatenid);
					newzutatenamounts.add(zutatenmenge);
				}
			}
			}
		}
		for(int i = 0; i < newzutatenids.size();i++) {
			if(newzutatenamounts.get(i)!=0) {
			s = c.createStatement();			
			sql = "INSERT INTO bestellungzutat(BESTELLNR, ZUTATENNR, MENGE) VALUES ("+bestellid+","+newzutatenids.get(i)+","+newzutatenamounts.get(i)+");";
			s.execute(sql);
			}
		}
		s = c.createStatement();
		sql = "UPDATE bestellung\r\n"
				+ "    SET RECHNUNGSBETRAG = "+rechnungsbetrag.doubleValue()+"\r\n"
				+ "    WHERE BESTELLNR = "+bestellid+";";
		s.execute(sql);
		s = c.createStatement();
		byte[] a = "done!".getBytes();
		c.close();
		return makeMeAnOutput(a);
	}
	
	@PostMapping(path="/Bestellungen")
	public ResponseEntity<byte[]> bestellungen (@RequestParam("kundenid") int kundenid) throws SQLException {
		Connection c = connect();
		Statement s = c.createStatement();
		
		String sql = "SELECT * FROM bestellung WHERE Kundennr="+kundenid+";";
		
		s.execute(sql);
		
		ResultSet r = s.getResultSet();
		ArrayList<byte[]> answerlist  = new ArrayList<byte[]>();
		while(r.next()) {
			if(r.getString("Rechnungsbetrag")!= null){
			answerlist.add(("Bestelldatum :"+r.getString("Bestelldatum")+" Rechnungsbetrag :"+r.getString("Rechnungsbetrag")+System.lineSeparator()).getBytes());
			}else {
				answerlist.add(("Bestelldatum :"+r.getString("Bestelldatum")+" Rechnungsbetrag : "+System.lineSeparator()).getBytes());
			}
		}
		byte[] a =delistify(answerlist);
		c.close();
		return makeMeAnOutput(a);
	}
	
	
	@PostMapping(path="/KundeExistiert")
	public ResponseEntity<byte[]> kundenid (@RequestParam("kundenid") int kundenid) throws SQLException {
		Connection c = connect();
		Statement s = c.createStatement();
		
		String sql = "SELECT Kundennr FROM Kunde WHERE Kundennr="+kundenid+";";
		
		s.execute(sql);
		
		ResultSet r = s.getResultSet();
		byte[] a = null;
		if(r.next()) {
			a = new byte[1];
			a[0] = 1;
		}
		c.close();
		return makeMeAnOutput(a);
	}
	
	public byte[] delistify(ArrayList<byte[]> a) {
		String oof="";
		byte[] ret = new byte[a.size()];
		for(int i = 0 ; i < ret.length;i++) {
			oof += new String(a.get(i));
		}
		ret = oof.getBytes();
		return ret;
	}
	
	
	public ResponseEntity<byte[]> makeMeAnOutput(byte[] data){
		   HttpHeaders responseHeaders = new HttpHeaders();
		   responseHeaders.set("MyResponseHeader", "MyValue");
		   return new ResponseEntity<byte[]>(data, responseHeaders, HttpStatus.OK);
	}
}
